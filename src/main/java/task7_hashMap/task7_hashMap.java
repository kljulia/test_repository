package task7_hashMap;

import java.sql.SQLOutput;
import java.util.*;

public class task7_hashMap
{
    public static void main(String[] arg)
    {

        HashMap<Integer,String> array = new HashMap<>();

        array.put(123,"1");
        array.put(1234,"2");
        array.put(12345,"3");


       Set<Integer> keys = array.keySet();
        Collection<String> values = array.values();
        for(Map.Entry<Integer, String> item : array.entrySet()){
            System.out.println("Key: " + item.getKey() + " Value: "
                    + item.getValue());        }

        System.out.println();

        HashSet<String> countryHashSet = new HashSet<>();
        countryHashSet.add("Франция");
        countryHashSet.add("Франция");


        Iterator<String> iter = countryHashSet.iterator();
        while (iter.hasNext()){
            System.out.println(iter.next());
        }
    }
}

