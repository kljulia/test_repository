package task_constructor;

import java.util.Scanner;

public class constructor {
    public  static void main(String[] arg) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the name: ");
        String name1 = in.nextLine();

        ConstructorClass constr = new ConstructorClass();
        ConstructorClass constr2=new ConstructorClass(name1);
    }
}
class ConstructorClass
{
    ConstructorClass(){
        System.out.println(this.getClass());
    }
    ConstructorClass(String name){
        System.out.println(this.getClass()+" made by "+name);
    }
}
