package task1_dataType;

import java.util.*;

public class list
{
    public static void main(String[] arg)
    {

        ArrayList<String> list = new ArrayList<String>();
        list.add("book");
        list.add("12.16$");
        list.add("7");

        ArrayList<String> list2 = new ArrayList<String>();
        list2.add("book2");
        list2.add("14000000$");
        list2.add("7000");

        ArrayList<String> list3 = new ArrayList<String>();
        list3.add("book3");
        list3.add("3000$");
        list3.add("300");

        HashMap<Integer, ArrayList> array = new HashMap<>();

        array.put(123, list);
        array.put(1234, list2);
        array.put(12345, list3);

        int m=0;

        while (true)
        {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter key: ");
            String keyEntered = in.nextLine();

            if (keyEntered.equals("exit"))
            {
                System.exit(0);
            }

            m++;


            int x=0;

            try {
                x = Integer.parseInt(keyEntered);
            } catch (Throwable t) { }

            if (array.containsKey(x))
            {
                System.out.println(array.get(x).get(1));
            } else
                {
                    System.out.println("key does not exist");
                }
        }
    }
}

