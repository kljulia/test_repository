package task1_dataType;

import java.util.Scanner;

public class arithmeticAverageArray {
    public static void main(String[] arg)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter array length: ");
        int size = in.nextInt();
        int numbers[] = new int[size];

        System.out.println("Insert array elements:");
        for (int i = 0; i < size; i++) {
            numbers[i] = in.nextInt();
        }

        double average = 0;
        if (numbers.length > 0)
        {
            double sum = 0;
            for (int j = 0; j < numbers.length; j++) {
                sum += numbers[j];
            }
            average = sum / numbers.length;
        }
        System.out.println("average is"+average);
    }
}
