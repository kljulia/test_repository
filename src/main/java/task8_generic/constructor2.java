package task8_generic;

public class constructor2 {
    public  static void main(String[] arg) {

        Gen<Integer> number = new Gen<Integer>(123);

        int value = number.getObject();
        number.showType();
        System.out.println("Значение " + value );

        Gen<String> stroka = new Gen<String>("Text");

        stroka.showType();

        String str = stroka.getObject();
        System.out.println("Значение: " + str);
    }
}
