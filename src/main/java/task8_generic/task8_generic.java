package task8_generic;

class Gen<newType>
{
    newType object;

    Gen(newType newObject) {
        object = newObject;
    }

    newType getObject() {

        return object;
    }

    void showType() {
        System.out.println("Тип: " + object.getClass().getName());
    }

}
