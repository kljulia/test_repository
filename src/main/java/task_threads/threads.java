package task_threads;

public class threads {
    public  static void main(String[] arg) {
        myClass object= new myClass();
        for (int i = 1; i < 3; i++){

            Thread t = new Thread(new CountThread(object));
            t.setName("Thread "+ i);
            t.start();
        }
    }
}

class myClass{

    int x;
    synchronized void method(){

           System.out.printf("%s started... \n", Thread.currentThread().getName());


            try{
                Thread.sleep(100);
            }
            catch(InterruptedException e){}

        System.out.printf("%s finished... \n", Thread.currentThread().getName());
    }
}

class CountThread implements Runnable{
    myClass res;
    CountThread(myClass res){
        this.res=res;
    }

    public void run(){
        res.method();
    }
}