package task_threads;

public class threads_array {
    public  static void main(String[] arg) {
        myClassArray object1= new myClassArray();
        for (int i = 1; i < 11; i++){

            Thread t1 = new Thread(new CountThread1(object1));
            t1.setName("Thread "+ i);
            t1.start();
        }
    }
}

class myClassArray{

    int x;
    synchronized void method1(){

        int[] array =new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.round((Math.random() * 10));
            System.out.print(array[i]+" ");
        }
        System.out.println();
    }
}

class CountThread1 implements Runnable{
    myClassArray res;
    CountThread1(myClassArray res){
        this.res=res;
    }

    public void run(){
        res.method1();
    }
}