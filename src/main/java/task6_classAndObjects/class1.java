package task6_classAndObjects;

public class class1 {
    public static void methodClass1()
    {
        publicMethod();
        privateMethod();
        protectedMethod();
    }

    public static void publicMethod() {
        Object instance = new class1();
        System.out.println( instance.getClass()+"public Method"); }
    private static void privateMethod() { System.out.println("private Method"); }
    protected static void protectedMethod() {
        System.out.println("protected Method");
    }
     static void defaultMethod() {
        System.out.println("default Method");
    }
}
