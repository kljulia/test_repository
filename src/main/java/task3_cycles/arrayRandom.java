package task3_cycles;

public class arrayRandom
{
    public static void main(String[] arg)
    {
        int[] array =new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.round((Math.random() * 10));
            System.out.print(array[i]+" ");
        }
        System.out.println();

        int q=0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 5) {
                q++;
            }
        }
        System.out.println("array consists '5' value "+q+" times");

    }
}
