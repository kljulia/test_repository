package task3_cycles;

import java.math.BigInteger;
import java.util.Scanner;

public class factorialImprove
{
    public static void main(String[] arg) {
        Scanner in = new Scanner(System.in);
        System.out.println("vvedite chislo: ");
        long vvedennoeChislo = in.nextInt();
        BigInteger c = BigInteger.valueOf(0);

        if (vvedennoeChislo > 0) {
            c = raschetFactoriala(vvedennoeChislo);

            System.out.println("task1_dataType.factorial is " + c);
        }
        else {
            System.out.println("chislo menshe 0");
        }
    }
        public static BigInteger raschetFactoriala (long number)
        {
            BigInteger result = BigInteger.valueOf(1);
            for (int i = 1; i <= number; i++) {
                result = result.multiply(BigInteger.valueOf(i)) ;
            }
            return result;

    }
}
