package task4_classAndObjects;

public class Animals {
    public  static void main(String[] arg) {

        Sobaka barbos = new Sobaka();
        barbos.Eat(); barbos.Heigh();barbos.Move();barbos.Weight();

        System.out.println();
        Dog gerasim = new Dog();
        gerasim.Eat();gerasim.Heigh();gerasim.Move();gerasim.Weight();

        System.out.println();
        Dog gera = new Dog();
        gera.Move("perekat"); gera.Weight(6);gera.Heigh(88);

    }

  public interface Zveri
    {
        void Weight();
        void Heigh();
        void Eat();
        void Move();
    }

  public static class Sobaka implements Zveri {
        public void Weight(){
            System.out.println("weight1");
        }
        public void Heigh(){
            System.out.println("heigh1");
        }
        public void Eat(){
            System.out.println("eat1");
        }
        public void Move(){
            System.out.println("move1");
        }
    }
    public static class Dog extends Sobaka{
        public void Weight(){
            System.out.println("weight2");
        }
        public void Heigh(){
            System.out.println("heigh2");
        }
        public void Eat(){
            System.out.println("eat2");
        }
        public void Move(){
            System.out.println("move2");
        }
        public void Move(String a){
            System.out.println("move is"+a);;
        }
        public void Weight(int q){
            System.out.println("weight is "+q);
        }
        public void Heigh(int w){
            System.out.println("heigh is "+w);
        }
    }
}
