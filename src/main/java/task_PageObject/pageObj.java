package task_PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.*;

public class pageObj
{
        WebDriver driver;

        By btnFeelLucky = By.name("btnI");
        By input = By.name("q");
        By btnSearch = By.name("btnK");


        public pageObj(WebDriver driver){
                this.driver=driver;
        }

        public void typeSearchWorld() {
                driver.findElement(input).sendKeys("kittens");
        }
        public void clicktheBtn() {
                driver.findElement(btnSearch).click();
        }
        public void search()
        {
                this.typeSearchWorld();
                this.clicktheBtn();
        }
}

